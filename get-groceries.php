<?php
include 'db.php';
$latitude = doubleval($_POST["latitude"]);
$longitude = doubleval($_POST["longitude"]);
$start = intval($_POST["start"]);
$results = $c->query("SELECT *, SQRT(POW(69.1 * (latitude - " . $latitude . "), 2) + POW(69.1 * (" . $longitude . " - longitude) * COS(latitude / 57.3), 2)) AS distance FROM groceries HAVING distance < 25 ORDER BY distance LIMIT " . $start . ",5;");
$groceries = [];
if ($results && $results->num_rows > 0) {
	while ($row = $results->fetch_assoc()) {
		array_push($groceries, $row);
	}
} else {
	$results = $c->query("SELECT * FROM groceries LIMIT " . $start . ",5");
	if ($results && $results->num_rows > 0) {
		while ($row = $results->fetch_assoc()) {
			array_push($groceries, $row);
		}
	}
}
echo json_encode($groceries);
